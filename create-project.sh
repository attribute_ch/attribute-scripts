#!/usr/bin/env bash

# call create-project.sh $PROJECT_NAME $DOMAIN

DIR="${0%/*}"
source "${DIR}"/functions.sh

if ([ $1 ]  && [ $2 ]); then
  PROJECT_NAME=$1
  DOMAIN=$2
  PROJECT_DIR=$PWD/$PROJECT_NAME
  DOCKER_YAML=$PROJECT_DIR/docker-compose.yml
  AMAZEEIO_YAML=$PROJECT_DIR/.lagoon.yml
  README=$PROJECT_DIR/README.md

  echo "creating $PROJECT_NAME..."

  # setup composer project
  composer create-project attributech/drupal-project:9.x-dev $PROJECT_NAME --stability dev --no-install --no-interaction

  # switch back to project directory
  cd ${PROJECT_DIR}

  git init
  git add -A
  git remote add origin git@gitlab.com:attribute/$PROJECT_NAME.git

  # files in /config/sync
  OLD=DRUPAL-PROJECT-SITEGROUP
  NEW="${DOMAIN/\./_}"
  find ${PROJECT_DIR} -type f | xargs sed -i "s/${OLD}/${NEW}/"

  # files in /config/sync
  OLD=DRUPAL-PROJECT-DOMAIN
  NEW=${DOMAIN}
  find ${PROJECT_DIR} -type f | xargs sed -i "s/${OLD}/${NEW}/"

  composer install
  composer drupal:scaffold

  docker-compose up -d

  docker-compose exec cli drush site:install minimal --existing-config;
  docker-compose exec cli drush user-login;

else
  echo "please supply project name and domain"
fi
