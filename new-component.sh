#!/bin/bash

cd "$PWD"
mkdir "$1"
cd "$1"
echo ".$1{}" >> _"$1".scss
echo "<div class=\""$1"\">random content</div>" >> "$1".twig
