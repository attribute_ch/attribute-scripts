#!/bin/bash

DIR="${0%/*}"
source "${DIR}"/functions.sh

develop_branch="develop";
commit_branch=$develop_branch
check_outdated=true

# Adding composer patch exit configuration
sed -i -z 's/}\n    }\n}/},\n        "composer-exit-on-patch-failure": true\n    }\n}/g' composer.json

git add .
git commit -m "Fixing patch exit configuration"



# update db
if d8-update.sh -n ; then
    echo "Update succeeded"
else
    echo "Update failed"
    exit 1
fi


# clear caches
if [ "$check_outdated" = true ] ; then
	# update composer dependencies
	if composer outdated "drupal/*"; then
	    echo "Composer updates available. Please install them first."
	else
	    echo "No composer updates available. Continuing..."
	fi
fi

ddrush cr ;

# update db
if ddrush -y updb ; then
    echo "drush updb succeeded"
else
    echo "drush updb failed"
    exit 1
fi

# export config
if ddrush -y cex ; then
    echo "drush cex succeeded"
else
    echo "drush cex failed"
    exit 1
fi

git add . ;
git commit -m "Updating config" ;

composer remove --dev webflo/drupal-core-require-dev behat/mink behat/mink-goutte-driver calderonzumba/gastonjs jcalderonzumba/mink-phantomjs-driver mikey179/vfsstream phpunit/phpunit symfony/css-selector

composer require drush/drush:^8.0 webflo/drupal-finder drupal/console --update-with-all-dependencies
composer remove drupal/core webflo/drupal-core-strict drupal-composer/drupal-scaffold webflo/drupal-finder webmozart/path-util webflo/drupal-core-require-dev oomphinc/composer-installers-extender vlucas/phpdotenv --no-update
composer require 'composer/installers:^1.7' drupal/core-composer-scaffold:^8.9 --no-update 
rm -rf vendor composer.lock 
composer require drupal/core-recommended:^8.9 --update-with-dependencies

git add .
git commit -m "Switching to drupal/core-recommended"

# update composer dependencies
if composer outdated "drupal/*"; then
    echo "Composer updates available. Please install them first."
else
    echo "No composer updates available. Continuing..."
fi

# Fixing install profile issue
sed -i 's,config_installer,minimal,g' web/sites/default/settings.php
sed -i 's,theme:,  minimal: 1000\ntheme:,g' web/sites/default/config/sync/core.extension.yml

git add .
git commit -m "Fixing install profile"




git push origin "$commit_branch"