#!/usr/bin/env bash

PROJECT_DIR=${PWD}
if ([ $1 ]  && [ $2 ]); then
  THEME_NAME=$1
  DOMAIN=$2

  THEME_TEMPLATE_DIR=$PROJECT_DIR/web/themes/contrib/starterkit/
  THEME_TARGET_DIR=$PROJECT_DIR/web/themes/${THEME_NAME}
  OLD_THEME_NAME=starterkit
  OLD_DOMAIN=starterkit
  echo "creating $THEME_NAME theme..."

  # copy template to project
  cp -R $THEME_TEMPLATE_DIR $THEME_TARGET_DIR

  cd $THEME_TARGET_DIR
  find $THEME_TARGET_DIR -name '*.DS_Store' -type f -delete
  rm -rf $THEME_TARGET_DIR/.git
  rm -rf $THEME_TARGET_DIR/.idea
  rename -f "s|${OLD_THEME_NAME}|${THEME_NAME}|" $THEME_TARGET_DIR/*
  LC_ALL=C find $THEME_TARGET_DIR -type f -exec sed -i "s/${OLD_THEME_NAME}/${THEME_NAME}/" {} +
  LC_ALL=C find $THEME_TARGET_DIR -type f -exec sed -i "s/${OLD_DOMAIN}/${DOMAIN}/" {} +
  npm install
  npm run gulp

else
  echo "please supply project name and domain"
fi
