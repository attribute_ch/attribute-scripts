#!/usr/bin/env bash

DIR="${0%/*}"
source "${DIR}"/functions.sh

down=false
no_interaction=false
develop_branch="develop";
commit_branch=$develop_branch
ignore_uncommitted_changes=false
sql_synced=false
clear_cache=false

print_usage() {
  printf "Usage:"
  printf "-d to stop and remove the container after updating."
  printf "-n to run updates without interaction."
  printf "-b on which branch to commit the changes on."
  printf "-i to ignore uncommitted changes."
  printf "-s do indicate that the sql database is already synced."
  printf "-c to clear cache on production before updating."
}

while getopts 'dnb:isc' flag; do
  case "${flag}" in
    d) down=true ;;
    n) no_interaction=true ;;
    b) commit_branch="${OPTARG}" ;;
    i) ignore_uncommitted_changes=true ;;
    s) sql_synced=true ;;
    c) clear_cache=true ;;
    *) print_usage
       exit 1 ;;
  esac
done

# exit if there are ignore uncommitted changes
if [ "$ignore_uncommitted_changes" = false ] ; then
  require_clean_work_tree
fi

# only checkout develop if neccessary
update_branch="develop";
current_branch=$(git symbolic-ref --short -q HEAD)

if [ "$current_branch" != "$update_branch" ]
then
  git checkout $update_branch
fi
git pull
git submodule sync
git submodule update --init

# checkout new branch if needed
if [ "$commit_branch" != "$develop_branch" ] ; then
  git checkout -b $commit_branch
fi

docker-compose up -d

# clear caches
if [ "$clear_cache" = true ] ; then
  ddrush @master cc all
fi

# pull master db
if [ "$sql_synced" = false ] ; then
  if [ "$no_interaction" = true ] ; then
    if ddrush sql-sync @master @self -y --create-db ; then
        echo "DB sync succeeded"
    else
        echo "DB sync failed"
        exit 1
    fi
  else
    if ddrush sql-sync @master @self --create-db ; then
        echo "DB sync succeeded"
    else
        echo "DB sync failed"
        exit 1
    fi
  fi
fi

d7-core-update.sh
git add .
git commit -m "Installing core updates"

d7-contrib-update.sh
git add .
git commit -m "Installing contrib updates"

# update db
if ddrush -y updb ; then
    echo "drush updb succeeded"
else
    echo "drush updb failed"
    exit 1
fi

git add . ;
git commit -m "Updating config" ;

ddrush pm-updatestatus --security-only;

# tear down
if [ "$down" = true ] ; then
  docker-compose down
fi

git push origin "$commit_branch"

# tear down
docker-compose down

# echo 'non regular modules:\n'
# git config --file .gitmodules --get-regexp url | awk '{print $2}' | grep -v 'git.drupal.org\|Entering'
