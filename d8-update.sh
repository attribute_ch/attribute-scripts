#!/bin/zsh
# Update to Drupal 8.3.1

DIR="${0%/*}"
source "${DIR}"/functions.sh

down=false
no_interaction=false
develop_branch="develop";
commit_branch=$develop_branch
ignore_uncommitted_changes=false
sql_synced=false
clear_cache=false
drush_command=ddrush

print_usage() {
  printf "Usage:"
  printf "-d to stop and remove the container after updating."
  printf "-n to run updates without interaction."
  printf "-b on which branch to commit the changes on."
  printf "-i to ignore uncommitted changes."
  printf "-s do indicate that the sql database is already synced."
  printf "-c to clear cache on production before updating."
}

while getopts 'dnb:isc' flag; do
  case "${flag}" in
    d) down=true ;;
    n) no_interaction=true ;;
    b) commit_branch="${OPTARG}" ;;
    i) ignore_uncommitted_changes=true ;;
    s) sql_synced=true ;;
    c) clear_cache=true ;;
    *) print_usage
       exit 1 ;;
  esac
done

# adapt drush command depending on amazee.io legacy and lagoon
# TBD: not working!
# if test -f .amazeeio.yml ; then
  # drush_command=ddrush
# else
  # drush_command="docker-compose exec cli drush"
# fi

# exit if there are ignore uncommitted changes
if [ "$ignore_uncommitted_changes" = false ] ; then
  require_clean_work_tree
fi

docker-compose up -d
current_branch=$(git symbolic-ref --short -q HEAD)
if [ "$current_branch" != "$develop_branch" ]
then
  git checkout $develop_branch
fi
git pull ;

# checkout new branch if needed
if [ "$commit_branch" != "$develop_branch" ] ; then
  git checkout -b $commit_branch
fi

# update composer dependencies
if composer update --with-all-dependencies; then
    echo "composer update succeeded"
else
    echo "composer update failed"
    exit 1
fi

git add . ;
git commit -m "Installing updates" ;

# clear caches
if [ "$clear_cache" = true ] ; then
  "$drush_command" @master cr
fi

# pull master db
if [ "$sql_synced" = false ] ; then
  if [ "$no_interaction" = true ] ; then
    if "$drush_command" sql-sync @master @self -y --create-db ; then
        echo "DB sync succeeded"
    else
        echo "DB sync failed"
        exit 1
    fi
  else
    if "$drush_command" sql-sync @master @self --create-db ; then
        echo "DB sync succeeded"
    else
        echo "DB sync failed"
        exit 1
    fi
  fi
fi

"$drush_command" cr ;

# update db
if "$drush_command" -y updb ; then
    echo "drush updb succeeded"
else
    echo "drush updb failed"
    exit 1
fi

# export config
if "$drush_command" -y cex ; then
    echo "drush cex succeeded"
else
    echo "drush cex failed"
    exit 1
fi

git add . ;
git commit -m "Updating config" ;

# tear down
if [ "$down" = true ] ; then
  docker-compose down
fi

git push origin "$commit_branch"