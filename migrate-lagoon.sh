#!/bin/bash

DIR="${0%/*}"
source "${DIR}"/functions.sh

ignore_uncommitted_changes=false

print_usage() {
  printf "Usage:"
  printf "-i to ignore uncommitted changes."
}
if ([ $1 ]  && [ $2 ]); then
  PROJECT_NAME=$1
  DOMAIN=$2
  PROJECT_DIR=$PWD

  echo "Migrating $PROJECT_NAME to lagoon..."

  # exit if there are ignore uncommitted changes
  if [ "$ignore_uncommitted_changes" = false ] ; then
    require_clean_work_tree
  fi

  if git_branch_exists lagoon-migration; then
    git checkout lagoon-migration
  else
    git checkout -b lagoon-migration
  fi

  rm .amazeeio.yml
  git add .amazeeio.yml
  git commit -m "Removing .amazeeio.yml"

  mv web/sites/default/config ./config
  git add ./config web/sites/default/config
  git commit -q -m "Moving config"

  cp ~/Sites/drupal-project/.lagoon.yml ./
  cp ~/Sites/drupal-project/docker-compose.yml ./
  cp -r ~/Sites/drupal-project/lagoon ./
  cp -r ~/Sites/drupal-project/scripts ./
  cp ~/Sites/drupal-project/web/sites/default/*.php ./web/sites/default
  cp ~/Sites/drupal-project/.gitignore ./
  git add .
  git commit -q -m "Adding lagoon files"

  # files in /config/sync
  OLD=DRUPAL-PROJECT-SITEGROUP
  NEW="${DOMAIN/\./-}"
  find ${PROJECT_DIR} -iname "*.yml" | xargs sed -i "s/${OLD}/${NEW}/"

  # files in /config/sync
  OLD=DRUPAL-PROJECT-DOMAIN
  NEW=${DOMAIN}
  find ${PROJECT_DIR} -iname "*.yml" | xargs sed -i "s/${OLD}/${NEW}/"
  git add .
  git commit -q -m "Replacing placeholders for lagoon"

  mkdir drush-legacy
  mv web/sites/default/aliases.drushrc.php ./drush-legacy
  git add .
  git commit -q -m "Adding drush aliases for legacy"

  rm web/sites/default/drushrc.php 
  git add .
  git commit -q -m "Removing old drushrc.php"

else
  echo "Please provide project name and domain"
fi