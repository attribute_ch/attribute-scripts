#!/usr/bin/env bash

DIR="${0%/*}"
source "${DIR}"/functions.sh

require_clean_work_tree

docker-compose exec -T --user drupal drupal bash -c "cd ~ && drush dl drupal-7 -y --drupal-project-rename='drupal-7'"
docker-compose exec -T --user drupal drupal bash -c "rm -rf ~/drupal-7/sites"
docker-compose exec -T --user drupal drupal bash -c "rm ~/drupal-7/.gitignore"
docker-compose exec -T --user drupal drupal bash -c "cp -R ~/drupal-7/* ~/public_html/"
