#!/bin/sh
# Update to Drupal 8.3.1

source ~/bin/attribute-scripts/functions.sh

no_interaction=false
develop_branch="develop";

print_usage() {
  printf "Usage:"
  printf "-n to run updates without interaction."
}

while getopts 'dnb:i' flag; do
  case "${flag}" in
    n) no_interaction=true ;;
    *) print_usage
       exit 1 ;;
  esac
done

docker-compose up -d
git checkout $develop_branch
git pull ;

# pull master db
if [ "$no_interaction" = true ] ; then
  dcbash drush sql-sync @master @self -y ;
else
  dcbash drush sql-sync @master @self ;
fi

# export config
if ddrush ups ; then
    echo "drush ups succeeded"
else
    echo "drush ups failed"
    exit 1
fi

# tear down
if [ "$down" = true ] ; then
  docker-compose down
fi

git push origin "$commit_branch"
