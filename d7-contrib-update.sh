#!/usr/bin/env bash

DIR="${0%/*}"
source "${DIR}"/functions.sh

require_clean_work_tree

docker-compose exec -T --user drupal drupal bash -c "drush pm-updatecode --security-only --no-core -y --check-updatedb=0";
