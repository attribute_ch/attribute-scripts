#!/bin/bash

exclude=''

successfulUpdates=();
failedUpdates=();

print_usage() {
  printf "Usage:"
  printf "Argument: type of site (d8, d7, lagoon)"
  printf "-f alternate sites text file"
  printf "-e exclude sites from updating"
}

while getopts 'f:e:' flag; do
  echo $flag
  case "${flag}" in
    f) sitesFileOption="${OPTARG}" ;;
    e) exclude="${OPTARG}" ;;
    *) print_usage
       exit 1 ;;
  esac
done

shift "$(($OPTIND -1))"

type=$1

# tear down
if [ "$sitesFileOption" ] ; then
  sitesFile=$sitesFileOption
else
  sitesFile=~/.drupal-sites/"$type".txt
fi

excludeArray=($exclude);

IFS=$'\r\n' read -d '' -r -a sites < "$sitesFile"

for site in "${sites[@]}";do  

  if [[ ! ${exclude[*]} =~ $site ]]
  then
    printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
    printf "Starting update on: $site\n"
    cd ~/Sites/"$site"
    printf ~/Sites/"$site\n"

    # clear caches
    # if migrate-core-recommended.sh ; then
    if "$type"-update.sh -nd ; then
      echo 'Update successful.'
      successfulUpdates+=($site)
    else
      echo 'Update failed!'
      failedUpdates+=($site)
    fi
  fi                                                         
done 


printf "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"
printf "Successful updates:\n"
printf '%s\n' "${successfulUpdates[@]}"

printf "\n\n"
printf "Failed updates:\n"
printf '%s\n' "${failedUpdates[@]}"