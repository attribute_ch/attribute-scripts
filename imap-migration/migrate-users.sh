#!/usr/bin/env bash

# runs https://github.com/imapsync/imapsync

file=$1
src="mf12o0305.sui-inter.net";
dst="imap.mail.hostpoint.ch";

{ while IFS=';' read usr pw; do
  imapsync --host1 "$src" --user1 "$usr" --password1 "$pw" \
           --host2 "$dst" --user2 "$usr" --password2 "$pw" \
           --ssl1 --ssl2
done ; } < $file
